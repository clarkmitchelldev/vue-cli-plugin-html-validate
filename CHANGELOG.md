# vue-cli-plugin-html-validate changelog

## [1.3.0](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v1.2.5...v1.3.0) (2020-11-08)

### Features

- html-validate v4 compatibility ([fab3d49](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/fab3d497b7a8a6eeddbb472ff7f9de076f5cbd09))

## [1.2.5](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v1.2.4...v1.2.5) (2020-11-01)

## [1.2.4](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v1.2.3...v1.2.4) (2020-10-25)

### Bug Fixes

- use html-validate@3 ([4a2c09c](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/4a2c09cb7c85fcdfb951eb9392ffd18bb91349cc))

## [1.2.3](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v1.2.2...v1.2.3) (2020-05-28)

### Bug Fixes

- `property default is not expected to be here` ([1eac4ec](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/1eac4ecd4b79e7bc9a33aea27bdd3cfa9a7a182e)), closes [html-validate#91](https://gitlab.com/html-validate/issues/91)

## [1.2.2](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v1.2.1...v1.2.2) (2020-03-29)

### Bug Fixes

- update html-validate-vue ([3dd1cfc](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/3dd1cfca1b069abcadba6b28d509df8ae68637c0))

## [1.2.1](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v1.2.0...v1.2.1) (2019-11-24)

### Bug Fixes

- fix typo in generated config ([5411fb1](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/5411fb19f51554c389e67725fb1e64cd0716e0bb))

# [1.2.0](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v1.1.1...v1.2.0) (2019-11-24)

### Features

- update to html-validate 2 and html-validate-vue 2 ([d34069f](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/d34069f0a6999923e59ed594b506d6be60cb4fea))

## [1.1.1](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v1.1.0...v1.1.1) (2019-11-18)

### Bug Fixes

- **deps:** update dependency @vue/cli-shared-utils to v4 ([3738361](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/3738361826a6507b568299ecbbeac95fa15c9fd7))

# [1.1.0](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v1.0.0...v1.1.0) (2019-09-28)

### Features

- adding generator ([f8dd880](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/f8dd880))
- generate .htmlvalidate.json ([96c020b](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/96c020b))
- support --config parameter ([42e07dd](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/42e07dd)), closes [#1](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/issues/1)

# 1.0.0 (2019-09-19)

### Features

- initial version ([73e0b2a](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/73e0b2a))
