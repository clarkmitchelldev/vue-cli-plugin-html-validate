module.exports = (api) => {
	api.registerCommand(
		"html-validate",
		{
			description: "validate html in source files",
			usage: "vue-cli-service html-validate [options] [...files]",
			options: {
				"--config [filename]": "use custom configuration file",
				"--formatter [formatter]": "specify formatter (default: codeframe)",
				"--max-warnings [limit]": "specify number of warnings to make build failed",
			},
			details: "For more options, see https://html-validate.org",
		},
		(args) => {
			require("./lint")(args, api);
		}
	);
};
