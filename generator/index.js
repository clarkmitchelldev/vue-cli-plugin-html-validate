module.exports = (api) => {
	api.extendPackage({
		scripts: {
			"html-validate": "vue-cli-service html-validate",
		},
		devDependencies: {
			"html-validate": "^4",
			"html-validate-vue": "^3",
		},
	});

	api.render(`./template`);
};
