/* eslint-disable no-console */

const DEFAULT_PATTERNS = ["src/**/*.vue", "src/**/*.html"];

/**
 * @returns {number}
 */
function getMaxWarnings(args) {
	return typeof args["max-warnings"] !== "undefined" ? args["max-warnings"] : -1;
}

function importDefault(mod) {
	return mod && mod.__esModule ? mod : { default: mod };
}

/* eslint-disable-next-line complexity */
module.exports = function lint(args = {}, api) {
	const cwd = api.resolve(".");
	const { log, done, exit, loadModule } = require("@vue/cli-shared-utils");
	const { HtmlValidate, CLI } = loadModule("html-validate", cwd, true) || require("html-validate");
	const { default: defaultConfig } = importDefault(
		loadModule("html-validate/dist/config/default", cwd, true) ||
			require("html-validate/dist/config/default")
	);

	const patterns = args._ && args._.length ? args._ : DEFAULT_PATTERNS;

	const cli = new CLI();
	const config = getConfig(cwd, args);
	const htmlvalidate = new HtmlValidate(config);
	const files = cli.expandFiles(patterns, { cwd });
	const formatter = cli.getFormatter(args.formatter || "codeframe");
	const report = htmlvalidate.validateMultipleFiles(files);

	const maxWarnings = getMaxWarnings(args);
	const isErrorsExceeded = report.errorCount > 0;
	const isWarningsExceeded = maxWarnings >= 0 && report.warningCount > maxWarnings;

	if (!isErrorsExceeded && !isWarningsExceeded) {
		if (!args.silent) {
			if (report.warningCount || report.errorCount) {
				console.log(formatter(report));
			} else {
				done(`No lint errors found!`);
			}
		}
	} else {
		console.log(formatter(report));
		if (isWarningsExceeded) {
			log(`html-validate found too many warnings (maximum: ${maxWarnings}).`);
		}
		exit(1);
	}

	function getConfig(cwd, args) {
		/* eslint-disable-next-line import/no-dynamic-require */
		return args.config ? require(`${cwd}/${args.config}`) : defaultConfig;
	}
};
